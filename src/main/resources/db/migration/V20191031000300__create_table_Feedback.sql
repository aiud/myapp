create table t_feedback(
                       feedback_id BIGSERIAL PRIMARY KEY,
                       message VARCHAR(150),
                       participant VARCHAR(20),
                       event_id          BIGINT references t_event(event_id)  ON DELETE CASCADE,
                       event_rate        NUMERIC(1) NOT NULL,
                       check (event_rate BETWEEN  1 AND 6)
);