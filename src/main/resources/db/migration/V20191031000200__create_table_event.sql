create table t_event(
                        event_id BIGSERIAL PRIMARY KEY,
                        event_name VARCHAR(200),
                        event_date TIMESTAMP,
                        description VARCHAR(200),
                        total_places INTEGER,
                        places_available INTEGER,
                        location VARCHAR(40),
                        event_photo bytea
);