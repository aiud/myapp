INSERT INTO t_event(event_id,event_name, event_date, description,total_places,places_available,location)
VALUES (100 , 'MedievalFest', '10-04-19 12:00:17', 'Medieval Party',100, 50,'Chisinau');
INSERT INTO t_event(event_id,event_name, event_date, description,total_places,places_available,location)
VALUES (101 , 'RetroDisco', '12-04-19 12:00:17', 'Retro disco party',200,50,'Chisinau');
INSERT INTO t_event(event_id,event_name, event_date, description,total_places,places_available,location)
VALUES (102 , 'ForestParty', '15-04-19 12:00:17', 'Best event, special quests',1000, 500,'Codru');
INSERT INTO t_event(event_id,event_name, event_date, description,total_places,places_available,location)
VALUES (103 , 'HarryPotter event', '18-04-19 12:00:17', 'Thematic party',205,50,'Chisinau');