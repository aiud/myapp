create table t_user(
                       user_id BIGSERIAL PRIMARY KEY,
                       user_name VARCHAR(20) UNIQUE ,
                       email VARCHAR(20) UNIQUE NOT NULL,
                       address VARCHAR(20),
                       phone_number VARCHAR(20),
                       encrypted_password VARCHAR(100),
                       user_photo bytea
);