package com.myProject.start.repository;

import com.myProject.start.models.AuthGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AuthGroupRepository extends JpaRepository<AuthGroup, Long> {

    List<AuthGroup> findByUserName(String name);

}
