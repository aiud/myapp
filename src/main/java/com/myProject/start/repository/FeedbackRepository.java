package com.myProject.start.repository;

import com.myProject.start.models.Feedback;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface FeedbackRepository extends JpaRepository<Feedback,Long > {

    List<Feedback> findAll();
    Optional<Feedback> findById(Long id);
    Feedback save(Feedback feedback);
}
