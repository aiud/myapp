package com.myProject.start.repository;

import com.myProject.start.models.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepo extends JpaRepository<User, Long> , JpaSpecificationExecutor<User> {

    Page<User> findAllByNameContainingIgnoreCase(String name, Pageable pageable);
    Optional<User> findByName(String name);
    User findByEmail(String email);
    void deleteById(Long id);
    List<User> findAll();
    List<User> findAllByEmailContainingIgnoreCase(String email, Pageable pageable);
}
