package com.myProject.start.repository;

import com.myProject.start.models.Event;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface EventRepo extends JpaRepository<Event, Long> {

    Optional<Event> findEventByEventName(String name);
    void deleteByEventName(String name);
    List<Event> findAll();

}
