package com.myProject.start.auth;

import com.myProject.start.models.AuthGroup;
import com.myProject.start.models.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.*;

public class LandonUserPrincipal implements UserDetails {

    private User user;
    private List<AuthGroup> authGroupsAuthGroupList;
    public LandonUserPrincipal(User user,List<AuthGroup> authGroupsAuthGroupList ){
        super();
        this.authGroupsAuthGroupList = authGroupsAuthGroupList;
        this.user = user;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
      if(null== authGroupsAuthGroupList){
          return  Collections.emptySet();
      }
      Set<SimpleGrantedAuthority> grantedAuthorities = new HashSet<>();
      authGroupsAuthGroupList.forEach(authGroup -> {
          grantedAuthorities.add(new SimpleGrantedAuthority(authGroup.getAuthGroup()));
      });
      return  grantedAuthorities;
    }

    @Override
    public String getPassword() {
        return this.user.getPassword();
    }

    @Override
    public String getUsername() {
        return this.user.getName();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
