package com.myProject.start.auth;

import com.myProject.start.models.AuthGroup;
import com.myProject.start.models.User;
import com.myProject.start.repository.AuthGroupRepository;
import com.myProject.start.repository.UserRepo;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class MyUserDetailsService implements UserDetailsService {


    private final UserRepo userRepository;
    private final AuthGroupRepository authGroupRepository;

    public  MyUserDetailsService(UserRepo userRepository, AuthGroupRepository authGroupRepository){
        super();
        this.userRepository = userRepository;
        this.authGroupRepository = authGroupRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        Optional<User> user = this.userRepository.findByName(userName);
              if(!user.isPresent()){
                  throw new UsernameNotFoundException("cannot find username: "+userName);
              }
              List <AuthGroup> authGroups = this.authGroupRepository.findByUserName(userName);

              return new LandonUserPrincipal(user.get(),authGroups);
    }
}
