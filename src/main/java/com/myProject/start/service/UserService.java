package com.myProject.start.service;

import com.myProject.start.DTO.SignUpDTO;
import com.myProject.start.exceptions.UserNotFoundException;
import com.myProject.start.models.User;
import com.myProject.start.repository.UserRepo;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

@Service
public class UserService {

    UserRepo userRepo;

    public UserService(UserRepo userRepo){
        this.userRepo = userRepo;
    }

    public List<User> findAllUsers() {
        return userRepo.findAll();
    }

    public Optional<User> findUserByName(String name){
        return  userRepo.findByName(name);
    }

    public void saveUser(@NotNull final SignUpDTO userDTO/**, MultipartFile file**/) throws IOException {


        User user = User.builder()
                .address(userDTO.getAddress())
                .email(userDTO.getEmail())
                .name(userDTO.getName())
                .phoneNumber(userDTO.getPhoneNumber())
                .password(BCrypt.hashpw(userDTO.getPassword(),BCrypt.gensalt()))
                /**.user_photo(compressBytes(file.getBytes()))**/
                .build();

        userRepo.save(user);
    }
/**
    public static byte[] compressBytes(byte[] data) {
        Deflater deflater = new Deflater();
        deflater.setInput(data);
        deflater.finish();
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
        byte[] buffer = new byte[1024];
        while (!deflater.finished()) {
            int count = deflater.deflate(buffer);
            outputStream.write(buffer, 0, count);
        }
        try {
            outputStream.close();
        } catch (IOException ignored) {
        }
        return outputStream.toByteArray();
    }

    public static byte[] decompressBytes(byte[] data) {
        Inflater inflater = new Inflater();
        inflater.setInput(data);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
        byte[] buffer = new byte[1024];
        try {
            while (!inflater.finished()) {
                int count = inflater.inflate(buffer);
                outputStream.write(buffer, 0, count);
            }
            outputStream.close();
        } catch (IOException | DataFormatException ignored) {
        }
        return outputStream.toByteArray();
    }

       /** return Optional.of(User.builder()
                .id(id)
                .address(user.getAddress())
                .email(user.getEmail())
                .name(user.getName())
                .phoneNumber(user.getPhoneNumber())
                .password(user.getPassword())
                .user_photo(decompressBytes(user.getUser_photo()))
                .build());
 }**/


    public void deleteUser(@NotNull Long id) {
        userRepo.deleteById(id);
    }

    public List<User> findAllUsersWithRolesSortingByUsernameContaining(
            String name,
            int pageNumber,
            int pageSize,
            Sort.Direction direction) {
        Sort sort = Sort.by(direction, "name");
        Pageable pageable = PageRequest.of(pageNumber, pageSize,sort);
        return  userRepo.findAllByNameContainingIgnoreCase(name, pageable).getContent();
    }

    public List<User> findAllUsersSortingByEmailContaining(
            String email,
            int pageNumber,
            int pageSize,
            Sort.Direction direction) {
        Sort sort = Sort.by(direction, "email");
        Pageable pageable = PageRequest.of(pageNumber, pageSize,sort);
        return  userRepo.findAllByEmailContainingIgnoreCase(email, pageable);
    }

    public Optional<User> findUserById(@NotNull final Long id) throws UserNotFoundException {
        return userRepo.findById(id);
    }

}
