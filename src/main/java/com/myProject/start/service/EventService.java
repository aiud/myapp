package com.myProject.start.service;

import com.myProject.start.DTO.EventDTO;
import com.myProject.start.DTO.EventUsersDTO;
import com.myProject.start.exceptions.EventNotFoundException;
import com.myProject.start.exceptions.UserNotFoundException;
import com.myProject.start.models.Event;
import com.myProject.start.models.User;
import com.myProject.start.repository.EventRepo;
import com.myProject.start.repository.UserRepo;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class EventService {

    EventRepo eventRepo;
    UserRepo userRepo;

    public EventService( EventRepo eventRepo,
                         UserRepo userRepo){
        this.eventRepo = eventRepo;
        this.userRepo = userRepo;
    }

    public void addUsersToEvent( EventUsersDTO eventUsers) {
        Optional<Event> event = eventRepo.findById(eventUsers.getEventId());
        if (!event.isPresent()) {
            throw new EventNotFoundException("event with id " + eventUsers.getEventId() + " not found");
        }
        List<User> users = event.get().getParticipants();
        List<Long> usersId = eventUsers.getUsersId();
        for (Long id : usersId) {
            Optional<User> user = userRepo.findById(id);
            if (!user.isPresent()) {
                throw new UserNotFoundException("User with id " + id + " not found");
            }

            users.add(user.get());
        }
        event.get().setParticipants(users);
        eventRepo.save(event.get());
    }

    public void save(@NotNull EventDTO eventDTO) {
        List<User> users = eventDTO.getUsersNames().stream()
                .map(userRepo::findByName)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList());

        Event event = Event.builder()
                .eventName(eventDTO.getEventName())
                .description(eventDTO.getDescription())
                .eventDate(eventDTO.getEventDate())
                .totalPlaces(eventDTO.getTotalPlaces())
                .placesAvailable(eventDTO.getAvPlaces())
                .location(eventDTO.getLocation())
                .participants(users)
                .build();

        eventRepo.save(event);
    }

    public List<Event> findAllEvents() {
        return eventRepo.findAll();
    }

    public Event findEventById(@NotNull Long id) {
        Optional<Event> event = eventRepo.findById(id);
        if (!event.isPresent()) {
            throw new EventNotFoundException("event with id " + id + " not found");
    }
        return event.get();
    }

    public void delete(String eventName) {
        eventRepo.deleteByEventName(eventName);
    }

    public void edit(@NotNull final EventDTO eventDTO, @NotNull final Long id) {
        List<User> users = eventDTO.getUsersNames().stream()
                .map(userRepo::findByName)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList());

        Event event = Event.builder()
                .eventId(id)
                .eventName(eventDTO.getEventName())
                .description(eventDTO.getDescription())
                .eventDate(eventDTO.getEventDate())
                .totalPlaces(eventDTO.getTotalPlaces())
                .placesAvailable(eventDTO.getAvPlaces())
                .location(eventDTO.getLocation())
                .participants(users).build();

        eventRepo.save(event);
    }
}
