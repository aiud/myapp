package com.myProject.start.service;

import com.myProject.start.DTO.FeedbackDTO;
import com.myProject.start.models.Event;
import com.myProject.start.models.Feedback;
import com.myProject.start.repository.FeedbackRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

@Service
public class FeedbackService {

    private  FeedbackRepository feedbackRepository;
    private  EventService  eventService;

    public FeedbackService(FeedbackRepository feedbackRepository,
                           EventService eventService){
        this.feedbackRepository = feedbackRepository;
        this.eventService = eventService;
    }

    public List<Feedback> getAll() {
        return feedbackRepository.findAll();
    }

    public Optional<Feedback> findById(Long id) {
        return feedbackRepository.findById(id);
    }

    public Feedback save(@NotNull Long id, @NotNull FeedbackDTO dto) {
        Event event = eventService.findEventById(id);


        Feedback feedback = Feedback.builder()
                .feedbackAuthor(dto.getUserName())
                .message(dto.getMessage())
                .rate(dto.getRate())
                .event(event)
                .build();

        return feedbackRepository.save(feedback);
    }
}
