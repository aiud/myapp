package com.myProject.start.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class SignInDTO {

    private String phoneNumber;
    private String email;

    @NotEmpty(message = "Introduce password")
    @NotNull(message = "Introduce password")
    private String password;

}
