package com.myProject.start.DTO;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FeedbackDTO {

    @NotNull
    @NotEmpty(message = "Please enter your name")
    private String userName;

    @NotNull
    @NotEmpty(message = "Please enter your feedback.")
    private String message;

    @NotNull
    private Integer rate;
}