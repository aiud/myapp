package com.myProject.start.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class EventUsersDTO {
    @NotNull(message = "Specify event id")
    private Long eventId;

    @NotNull(message = "Specify users id")
    @NotEmpty(message = "Specify users id")
    private List<Long> usersId;
}
