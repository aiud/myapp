package com.myProject.start.DTO;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;

import javax.persistence.Lob;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class EventDTO  {

    @NotNull(message = "Event name cannot be null")
    @NotEmpty(message = "Please specify event name")
    @Size(min = 1, max = 50, message = "Event name must be between 1 and 50 characters")
    private String eventName;

    @NotNull(message = "Specify date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime eventDate;

    @NotNull(message = "Event name cannot be null")
    @NotEmpty(message = "Please specify event name")
    @Size(min = 10, max = 300, message = "Description must be between 10 and 300 characters")
    private String description;

    @NotNull(message = "Specify number of total places")
    private Integer totalPlaces;

    @NotNull(message = "specify userNames")
    private List<String> usersNames;

    @JsonIgnore
    public Integer getAvPlaces(){
        return ((usersNames.size()==0)? totalPlaces : totalPlaces-usersNames.size());
    }

    @NotNull(message = "Location cannot be null")
    @NotEmpty(message = "Please specify location")
    @Size(min = 2, max = 50, message = "Event name must be between 1 and 50 characters")
    private String location;

    @Lob
    @Type(type="org.hibernate.type.BinaryType")
    private byte[] event_photo;
}