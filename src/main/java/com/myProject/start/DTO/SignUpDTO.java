package com.myProject.start.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;

import javax.persistence.Lob;
import javax.validation.constraints.*;

    @NoArgsConstructor
    @AllArgsConstructor
    @Data
    public class SignUpDTO {

        @NotNull(message = "Name cannot be null.")
        @Size(min = 3, max = 20, message = "Name must be between 3 and 20 characters")
        private String name;

        @NotNull(message = "you didn't specified phone number")
        private String phoneNumber;
        @NotNull(message = "you didn't specified address")
        private String address;

        @NotNull
        @NotEmpty
        @Email
        private String email;

        @NotNull(message = "Password cannot be null.")
        @Size(min = 6, message = "Password must be min 6 characters")
        @Pattern(message = "Password must begin with ^ and must contain minimum 1 digit, uppercase, lowercase letter, special character",
                regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{6,}$")
        private String password;

    }

