package com.myProject.start.controllers;

import com.myProject.start.DTO.EventDTO;
import com.myProject.start.DTO.EventUsersDTO;
import com.myProject.start.DTO.FeedbackDTO;
import com.myProject.start.exceptions.UserNotFoundException;
import com.myProject.start.models.Event;
import com.myProject.start.models.User;
import com.myProject.start.service.EventService;
import com.myProject.start.service.FeedbackService;
import com.myProject.start.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.OK;

@RestController
public class EventController {

    EventService eventService;
    UserService userService;
    FeedbackService feedbackService;

    public EventController( EventService eventService,
                            UserService userService,
                            FeedbackService feedbackService){
        this.feedbackService = feedbackService;
        this.userService = userService;
        this.eventService = eventService;
    }

    @GetMapping("/events")
    public ResponseEntity<?> getAllEvents() {
        List<Event> response = eventService.findAllEvents();
        return response.isEmpty() ? ResponseEntity.status(NOT_FOUND).body("There are no events yet.")
                : ResponseEntity.ok().body(response);
    }

    @GetMapping("/events/{id}")
    public ResponseEntity<?> getById(@PathVariable Long id) {
        Optional<Event> event = Optional.ofNullable(eventService.findEventById(id));
        return !event.isPresent() ? ResponseEntity.status(NOT_FOUND).body("Event with id " + id + " not found") :
                ResponseEntity.ok(event.get());
    }

    @PostMapping("/events")
    public ResponseEntity<?> createEvent(@RequestBody @Valid EventDTO event) {
        List<String> userNames = event.getUsersNames();
        for (String userName : userNames) {
            Optional<User> user = userService.findUserByName(userName);
            if (!user.isPresent()) {
                throw new UserNotFoundException("user with Name" + userName + "does not exist");
            }
        }
        eventService.save(event);
        return ResponseEntity.status(OK).body("Successful event registration!");
    }
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PutMapping("/events/{id}")
    public ResponseEntity<?> updateEvent(@RequestBody @Valid EventDTO event,
                                         @PathVariable("id") Long id) {
        List<String> userNames = event.getUsersNames();
        for (String userName : userNames) {
            Optional<User> user = userService.findUserByName(userName);
            if (!user.isPresent()) {
                throw new UserNotFoundException("user with Name" + userName + "does not exist");
            }
        }

        eventService.edit(event, id);
        return ResponseEntity.status(OK).body("Event updated!");
    }

    @PostMapping("/events/addUsers")
    public ResponseEntity<?> addUsersToEvent(@Valid @RequestBody EventUsersDTO eventUsers) {
        eventService.addUsersToEvent(eventUsers);
        return ResponseEntity.status(OK).body("Users added to event !");
    }

    @PostMapping("events/addFeedback/{id}")
    public ResponseEntity<?> addFeedback(@PathVariable Long id, @Valid @RequestBody FeedbackDTO dto) {
        feedbackService.save(id, dto);
        return ResponseEntity.status(OK).body("Feedback successfully added!");
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(HttpMessageNotReadableException.class)
    public String handleMessageNotReadableException(
            HttpMessageNotReadableException ex) {
        return ex.getLocalizedMessage();
    }

}


