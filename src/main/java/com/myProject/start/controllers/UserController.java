package com.myProject.start.controllers;

import com.myProject.start.DTO.SignUpDTO;
import com.myProject.start.exceptions.BadRequestException;
import com.myProject.start.exceptions.UserNotFoundException;
import com.myProject.start.models.Event;
import com.myProject.start.models.User;
import com.myProject.start.service.UserService;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.OK;

@RestController
public class UserController {

    UserService service;

    public UserController(UserService service) {
        this.service = service;
    }

    @GetMapping("/users/name/{name}")
    public ResponseEntity<?> findAllUsersWithNameContaining(@PathVariable("name") String name,
                                                  @RequestParam("pageNumber") Integer pageNumber,
                                                  @RequestParam("pageSize") Integer pageSize,
                                                  @RequestParam("sort") String sort) {
        return Optional.ofNullable(sort)
                .map(String::toUpperCase)
                .flatMap(Sort.Direction::fromOptionalString)
                .map(direction -> service.findAllUsersWithRolesSortingByUsernameContaining(name, pageNumber, pageSize, direction))
                .map(ResponseEntity::ok)
                .orElseThrow(BadRequestException::new);
    }

    @GetMapping("/users/email/{email}")
    public ResponseEntity<?> findAllUsersWithEmailContaining(@PathVariable("email") String email,
                                                   @RequestParam("pageNumber") Integer pageNumber,
                                                   @RequestParam("pageSize") Integer pageSize,
                                                   @RequestParam("sort") String sort) {
        return Optional.ofNullable(sort)
                .map(String::toUpperCase)
                .flatMap(Sort.Direction::fromOptionalString)
                .map(direction -> service.findAllUsersSortingByEmailContaining(email, pageNumber, pageSize, direction))
                .map(ResponseEntity::ok)
                .orElseThrow(BadRequestException::new);
    }

    @GetMapping("/users/isUserSubscribed/{username}/{eventname}")
    public ResponseEntity<?> isUserSubscribedToEvent(@PathVariable("username") String username,
                                                     @PathVariable("eventname") String eventname) {
        Optional<User> user = service.findUserByName(username);

        return user.isPresent() ?
                ResponseEntity.status(OK).body(user.map(User::getEvents)
                        .get().stream().map(Event::getEventName)
                        .collect(Collectors.toList()).contains(eventname)) :
                ResponseEntity.status(HttpStatus.NOT_FOUND).body(false);

    }

    @GetMapping("/users")
    public ResponseEntity<?> getAllUsers() {
        List<User> users = service.findAllUsers();
        return users.isEmpty() ?
                ResponseEntity.status(HttpStatus.NOT_FOUND).body("There are no users")
                : ResponseEntity.status(OK).body(users);
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<?> getById(@PathVariable Long id) {

        Optional<User> user = service.findUserById(id);

        return !user.isPresent() ?
                ResponseEntity.status(NOT_FOUND).body("User with id " + id + " not found")
                : ResponseEntity.status(OK).body(user.get());
    }

    @PostMapping("/users/signUp"/** consumes = MediaType.MULTIPART_FORM_DATA_VALUE**/)
    public ResponseEntity<?> signUp(
           /** @RequestPart("file") MultipartFile file,**/
            @Valid @RequestBody SignUpDTO user) throws IOException {
        service.saveUser(user);
        return ResponseEntity.status(OK).body("Successful registration");
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @DeleteMapping("/users/{id}")
    public ResponseEntity<?> deleteUser(@PathVariable Long id) {
        Optional<User> user = service.findUserById(id);
        if (!user.isPresent()) {
            throw new UserNotFoundException("User not found");
        }
        service.deleteUser(id);
        return ResponseEntity.status(OK).body("User deleted");
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }

}
