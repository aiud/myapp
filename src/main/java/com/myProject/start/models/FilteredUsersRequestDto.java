package com.myProject.start.models;


import lombok.Builder;
import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@ToString
@Builder
public class FilteredUsersRequestDto {
    String name;
    List<Event> events;
}
