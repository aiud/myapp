package com.myProject.start.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor

@Entity
@Builder
@ToString(exclude = "participants")
@EqualsAndHashCode(exclude = "participants")
@Data
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "t_event")
public class Event {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "event_id")
    private Long eventId;

    @Lob
    @Type(type = "org.hibernate.type.BinaryType")
    private byte[] event_photo;

    @Column(name = "event_name")
    private String eventName;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    @JsonFormat(pattern = "YYYY:MM:dd HH:mm")
    @Column(name = "event_date")
    private LocalDateTime eventDate;

    @Column(name = "description")
    private String description;

    @Column(name = "total_places")
    private Integer totalPlaces;

    @Column(name = "places_available")
    private Integer placesAvailable;

    @Column(name = "location")
    private String location;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "t_user_event", joinColumns = @JoinColumn(name = "event_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id"))
    private List<User> participants;

    @OneToMany(mappedBy = "event", fetch = FetchType.LAZY)
    private List<Feedback> feedbackList;

}
