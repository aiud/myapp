package com.myProject.start.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "t_auth")
public class AuthGroup {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "auth_user_group_id")
    private Long id;

    @Column(name = "username")
    private String userName;

    @Column(name = "auth_group")
    private Sktring authGroup;
}