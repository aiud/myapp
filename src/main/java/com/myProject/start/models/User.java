package com.myProject.start.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Entity
@Data
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@ToString(exclude = "events")
@EqualsAndHashCode(exclude="events")
@Builder
@Table(name = "t_user")
public class User  {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY )
    @Column(name = "user_id")
    private Long id;

    @Column(name = "user_name")
    private String name;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "address")
    private String address;

    @Column(name = "email")
    private String email;

    @Column(name = " encrypted_password")
    private String password;

    @JsonIgnore
    @ManyToMany(mappedBy = "participants", fetch = FetchType.LAZY)
    private List<Event> events;

    @Lob
    @Type(type="org.hibernate.type.BinaryType")
    private byte[] user_photo;


}
