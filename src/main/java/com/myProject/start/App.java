package com.myProject.start;

import com.myProject.start.DTO.EventDTO;
import com.myProject.start.DTO.SignUpDTO;
import com.myProject.start.models.AuthGroup;
import com.myProject.start.repository.AuthGroupRepository;
import com.myProject.start.service.EventService;
import com.myProject.start.service.UserService;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


@SpringBootApplication
public class App 
{
    public static void main( String[] args )
    {
        SpringApplication.run(App.class);
    }

    @Bean
    ApplicationRunner applicationRunner(UserService userService, EventService eventService, AuthGroupRepository authGroupRepository){
        List<String> participants = new ArrayList<>();
        participants.add("Alex Dimov");

        return  args -> {
            userService.saveUser(new SignUpDTO
                    ("Alex Dimov", "+37368436510","str.Crasescu 62","aiud@gmail.com","password"));
            userService.saveUser(new SignUpDTO
                    ("Cristian Capcelea", "+37368432510","str.Studentilor 12","capcelea@gmail.com","password"));
            userService.saveUser(new SignUpDTO
                    ("Dan Caldare", "+37368436987","str.Doina 62","caldare@gmail.com","password"));
            userService.saveUser(new SignUpDTO
                    ("Inga Starniciuc", "+3736865610","str.Crasescu 62","starniciuc@gmail.com","password"));
            eventService.save(new EventDTO("Corona virus party", LocalDateTime.of(2020,6,15,6,15 ),
                    "Awesome party",120,participants,"Chisinau - Moldexpo",null));
            eventService.save(new EventDTO("Night movie", LocalDateTime.of(2020,6,16,6,15 ),
                    "Awesome party",120,participants,"Chisinau - FabLab",null));
            eventService.save(new EventDTO("Anime party", LocalDateTime.of(2020,6,17,6,15 ),
                    "Awesome party",120,participants,"Chisinau - Tekwil",null));
            eventService.save(new EventDTO("Harry Potter party", LocalDateTime.of(2020,6,18,6,15 ),
                    "Awesome party",120,participants,"Chisinau - Ellation",null));
            eventService.save(new EventDTO("Opera", LocalDateTime.of(2020,6,19,6,15 ),
                    "Awesome party",120,participants,"Chisinau - teatrul Mihai Eminescu",null));
            authGroupRepository.save(new AuthGroup(1L,"Alex Dimov","ADMIN"));
            authGroupRepository.save(new AuthGroup(2L,"Cristian Capcelea","ADMIN"));
        };
    }

}
