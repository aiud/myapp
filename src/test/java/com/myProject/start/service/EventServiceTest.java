package com.myProject.start.service;

import com.myProject.start.DTO.EventDTO;
import com.myProject.start.DTO.EventUsersDTO;
import com.myProject.start.exceptions.EventNotFoundException;
import com.myProject.start.exceptions.UserNotFoundException;
import com.myProject.start.models.Event;
import com.myProject.start.models.Feedback;
import com.myProject.start.models.User;
import com.myProject.start.repository.EventRepo;
import com.myProject.start.repository.UserRepo;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;
import java.util.*;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class EventServiceTest {


    private EventRepo eventRepository = mock(EventRepo.class);
    private UserRepo userRepository = mock(UserRepo.class);
    private EventService eventService = new EventService(eventRepository, userRepository);

    List<User> participants = new ArrayList<>();
    List<String> participantsFromDTO = new ArrayList<>();
    List<Event> events = new ArrayList<>();
    List<Long> ids = new ArrayList<>();
    Event eventFromDb;
    EventDTO eventDTORegister;
    EventUsersDTO eventUsers;
    User user;

    @Before
    public void beforeAllSetup(){
        user = new User(UUID.randomUUID().getLeastSignificantBits(), "Ragnar Lodbrock","068532545", "str.V.Badiu 55","lodbrock@mail.ru","pass" ,null, null);
        eventDTORegister = new EventDTO("Harry Potter party", LocalDateTime.of(2020,6,18,6,15 ),
                "Awesome party",120, participantsFromDTO,"Chisinau - Ellation",null);
        eventFromDb = new Event(UUID.randomUUID().getLeastSignificantBits() ,null, "HarryPotter event", LocalDateTime.of(2001,5,6,5,6,5),
                "Thematic party", 205 , 50,"Chisinau", participants , new ArrayList<Feedback>());
        events.add(eventFromDb);
        ids.add(1L);
        eventUsers = new EventUsersDTO(10L, ids);
    }


    @Test
    public void findAllTest(){
        when(eventRepository.findAll()).thenReturn(events);
        List<Event> findEvents = eventService.findAllEvents();
        assertEquals(findEvents.get(0), eventFromDb);
        assertFalse(findEvents.isEmpty());
    }

    @Test
    public void saveEventFromDTO(){
        Event event =  Event.builder()
                .eventId(2L)
                .eventName(eventDTORegister.getEventName())
                .description(eventDTORegister.getDescription())
                .eventDate(eventDTORegister.getEventDate())
                .totalPlaces(eventDTORegister.getTotalPlaces())
                .placesAvailable(eventDTORegister.getAvPlaces())
                .location(eventDTORegister.getLocation())
                .build();
           when(eventRepository.save(event)).thenReturn(event);
           eventService.save(eventDTORegister);
    }

    @Test
    public void testFindEventById(){
        when(eventRepository.findById(anyLong())).thenReturn(Optional.ofNullable(eventFromDb));
        Optional<Event> event = Optional.ofNullable(eventService.findEventById(2L));
        assertTrue(event.isPresent());
    }

    @Test
    public void testFindEventByIdException() throws  EventNotFoundException{
        when(eventRepository.findById(anyLong())).thenReturn(Optional.empty() );
        assertThrows(EventNotFoundException.class,() -> eventService.findEventById(2L));
    }

    @Test
    public void addUsersToEventTest(){
        when(eventRepository.findById(anyLong())).thenReturn(Optional.ofNullable(eventFromDb));
        when(userRepository.findById(anyLong())).thenReturn(Optional.ofNullable(user));
        eventService.addUsersToEvent(eventUsers);

    }

    @Test
    public void addUsersToEventTestThrowEventNotFound() throws EventNotFoundException{
        when(eventRepository.findById(anyLong())).thenReturn(Optional.empty());
        assertThrows(EventNotFoundException.class, () -> eventService.addUsersToEvent(eventUsers));
    }

    @Test
    public void addUsersToEventTestThrowUserNotFound() throws EventNotFoundException{
        when(eventRepository.findById(anyLong())).thenReturn(Optional.ofNullable(eventFromDb));
        when(userRepository.findById(anyLong())).thenReturn(Optional.empty());
        assertThrows(UserNotFoundException.class, () -> eventService.addUsersToEvent(eventUsers));
    }
}
